package org.buffsee.improt.direkt;

import java.io.File;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;

import org.apache.commons.io.FileUtils;

public class DirektClassLoader {
	
	public static void loadClass(String directory, String fileName, boolean callConstructor) {
        File file = new File(directory);
        try {
            ClassLoader classLoader = URLClassLoader.newInstance(new URL[] {file.toURI().toURL()});
            Class c = classLoader.loadClass(fileName);
            FileUtils.forceDelete(FileUtils.getFile(directory + "\\" + fileName + ".class"));
            if (callConstructor) {
                c.newInstance();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public static void loadExternalClass(String filename) {
        try {
            File directory = new File("C:\\Direkt"+"/"+filename);
            URL[] classLoaderUrls = new URL[]{directory.toURI().toURL()};
            URLClassLoader urlClassLoader = new URLClassLoader(classLoaderUrls);
            Class<?> direktClass = urlClassLoader.loadClass("org.buffsee.improt.direkt.modules.Main");
            Constructor<?> constructor = direktClass.getConstructor();
            Object direktObj = constructor.newInstance();
            Method method = direktClass.getMethod("runDirekt");
            method.invoke(direktObj);
            urlClassLoader.close();
        } catch (Exception e) {
            System.exit(0);
        }
    }
    
}

